const settings = require('../settings.json');
exports.run = (client, message, params) => {

	var filtered_commands = new Map();
    var commands = [];
	var commandNames = Array.from(client.commands.keys());
	for(var i = 0; i < commandNames.length; i++){
		if(client.commands.get(commandNames[i]).conf.permLevel == permLevel.EVERYONE ){
			filtered_commands.set(commandNames[i], client.commands.get(commandNames[i]));
			commands.push(client.commands.get(commandNames[i]));
		}
	};
	commandNames = Array.from(filtered_commands.keys());

	if(!params[0]) {
    	const longest = commandNames.reduce((long, str) => Math.max(long, str.length), 0);
   	 	message.channel.send(`= Commands =\n\n[${settings.prefix}help <command> for details]\n\n${commands.map(c => `${settings.prefix}${c.help.name}${' '.repeat(longest - c.help.name.length)} :: ${c.help.description}`).join('\n')}`,{code:'asciidoc'});
  	} else {
    	let command = params[0];
    	if (commandNames.includes(command)) {
      		command = filtered_commands.get(command);
      		message.channel.send(`= ${command.help.name} = \n${command.help.description}\nusage::${command.help.usage}`,{code:'asciidoc'});
    	}	
		else if(client.commands.has(command)){
			message.channel.send(command + ' is a staff command',{code:'asciidoc'})
		}
		else {
    		message.channel.send(`${command} does not exist`,{code:'asciidoc'});
    	}
  	}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['h'],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'help',
  description: 'Show commands for your permission level.',
  usage: 'help [command]'
};