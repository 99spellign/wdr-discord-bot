exports.run = async function(client, message, args){

    let users = message.mentions.members;
    var user_ids = Array.from(users.keys());
    for(var i = 0; i < args.length; i++){
        if( /^\d+$/.test(args[i]))
            user_ids.push(args[i])
    }
    if(user_ids.length === 0){
        message.channel.send("Please ping at least 1 user to have a UD with");
        return;
    }
    let perms = [
        {//mod
            id: "408106863117336577",
            allow: ['VIEW_CHANNEL']
        },
        {//trial mod
            id : "423653735449886720",
            allow : ['VIEW_CHANNEL']
        },
        {//everyone
            id : message.guild.roles.everyone,
            deny : ['VIEW_CHANNEL']
        },
        {//admin bots
            id : "482771653273714688",
            allow : ['VIEW_CHANNEL']
        },
        {//Twisty
            id : "228019028755611648",
            allow : ['VIEW_CHANNEL']
        }
        ];
    for(var i = 0; i < user_ids.length; i++){//mentions
        try {
            perms.push({id: user_ids[i], allow: ['VIEW_CHANNEL']})
        }
        catch (e){
            console.log(e);
        }
    }
    let channel = await message.guild.channels.create(args[0].replace(/ /g, "-") , {type: 'text', parent:'481378650877329417', permissionOverwrites:perms});
    message.channel.send(channel.toString() + " created")
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'ud',
  description: '',
  usage: 'ud channel_name @user1 @user2 ...'
};
