exports.run = function(client, message){
    var verzik_role = message.guild.roles.cache.get("556557558761127936");
    if(message.member.roles.cache.has(verzik_role.id)){
      message.member.roles.remove(verzik_role).catch(console.error);
      message.channel.send("<@!" + message.member.id + "> Verzik role removed");
    }else{
      message.channel.send("<@!" + message.member.id + "> you do not have the Verzik role");
    }
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'remove-verzik',
  description: 'Removes your own Verzik role',
  usage: 'remove-verzik'
};
