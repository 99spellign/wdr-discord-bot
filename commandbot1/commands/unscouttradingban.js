exports.run = async function(client, message, args) {
  const {google} = require("googleapis"); //npm install googleapis@39 --save
  const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well

  const google_client = new google.auth.JWT(
    google_sheet_keys.client_email,
    null,
    google_sheet_keys.private_key,
    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
  );

  google_client.authorize(function(error, tokens){
    if(error){
      console.log(error);
      return;
    }
  });
  const google_sheet = google.sheets({version:"v4", auth:google_client})
  const opt1= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Scout_trading_bans"
  }
  var data = await google_sheet.spreadsheets.values.get(opt1);
  var bans = data.data.values;
  //above is sheets stuff


  await message.guild.members.fetch();
  let user = client.users.resolve(args[0]);
  var user_ids;
  if(user == null){
    let users = message.mentions.members;
    user_ids = Array.from(users.keys());
  }
  else{
    user_ids = [user.id];
  }
  if(user_ids.length > 1 || user_ids.length === 0){
    message.channel.send("Please ping exactly 1 user to unban from scout trading. If you believe this is an error please double check they're still in wdr");
    return;
  }
  var userId = user_ids[0];
  let row = -1;
  for(let i = 0; i < bans.length; i++){
    if(bans[i][0] === userId)
      row = i;
  }
  if(row === -1){
    message.channel.send("This user is not banned from <#530829059048079360>.");
    return;
  }
  message.channel.send("<@!"+userId+"> has been unbanned from <#530829059048079360>");

  let offences = message.guild.channels.cache.find(channel => channel.id === "408119551566282752");

  let offence = "Unbanned from scout trading";
  let punishment = "Scout trading unban";
  let totalStrikes = "N/A";

  let msg = "**Discord Name:** <@!"+userId+">\n";//single line break
  msg += "**Discord ID:** "+userId+"\n\n";//Double line break here
  msg += "**Offence:** "+offence+"\n\n"; //Another double line break required here
  msg += "**Punishment:** "+punishment+"\n"; //Single line break
  msg += "**Total Strikes:** "+totalStrikes+"\n"; //One more line break
  msg += "**Issued by:** <@!"+message.member.id+">\n";
  msg += "━━━━━━━━━━━━━━━━━━";

  offences.send(msg);
  const opt2= {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    resource: {"requests":
        [
          {
           "deleteRange":
            {
               "range":
                 {
                    "sheetId":1096849522,
                   "startRowIndex": row,
                   "endRowIndex": row+1
                 },
              "shiftDimension": "ROWS"
             }
          }
        ]
    }
  }

  let userToAddRolesTo = await message.guild.members.fetch(userId);
  userToAddRolesTo.roles.add("828750677701558312");
  var res = await google_sheet.spreadsheets.batchUpdate(opt2);
}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["unstb"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "un-scout-trading-ban",
  description: "unbans user from scout trading",
  usage: "un-scout-trading-ban @user"
};