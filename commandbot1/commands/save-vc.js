const fs = require('fs');
exports.run = function(client, message) {
  if (!message.member.voice.channel) {
    message.channel.send(`You must be in the voice channel you want to save the users from`);
    return;
  }
  let vc = message.member.voice.channel;

  if(!vc.permissionsFor(message.member).has("MOVE_MEMBERS")){
    message.channel.send(`You must have move members permission in the voice channel you want to use this in`);
    return;
  }
  let members = vc.members;

  let vcJson = {
    "id": vc.id,
    "members": Array.from(members.keys()),
    "expiry": Date.now()+5400000,
    "RL": message.member.id
  };
  let contents = "{}";
  try {
    if (fs.existsSync("vcs.json")){
      contents = fs.readFileSync("vcs.json", "utf-8");
    }
  }
  catch (e){
    console.log(e);
  }
  let json = JSON.parse(contents);

  let vcFromJson = -1;

  let elements = json.length

  for(let i = 0; i < elements; i++){
    if(json[i].RL === message.member.id){
      json.splice(i, 1);
      if(i === --elements){
        break;
      }
    }
    if(json[i].id === vc.id){
      vcFromJson = i;
    }
  }

  if(vcFromJson === -1){
    json.push(vcJson);
  }
  else{
    json[vcFromJson] = vcJson;
  }

  fs.writeFile('vcs.json', JSON.stringify(json) , (err) => {
    if (err) {
      throw err
    }
  });

  message.channel.send("VC saved, if someone disconnects they can type !rejoin to rejoin");

}
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["svc"],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "save-vc",
  description: "Saves the people in your current vc so the bot can move people back if they dc",
  usage: 'USAGE: !save-vc'
};
