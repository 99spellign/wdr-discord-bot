exports.run = function(client, message){
  message.channel.send("Ping?")
    .then(msg=> {
      msg.edit(`Pong! (took: ${msg.createdTimestamp - message.createdTimestamp}ms)`);
    });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["p"],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "ping",
  description: "Pings bot",
  usage: "ping"
};
