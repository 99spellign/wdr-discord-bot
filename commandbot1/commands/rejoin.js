const fs = require('fs');
exports.run = function(client, message) {
  if (!message.member.voice.channel) {
    message.channel.send(`You must be in the voice channel to use this command`);
    return;
  }


  let contents = "{}";
  try {
    if (fs.existsSync("vcs.json")){
      contents = fs.readFileSync("vcs.json", "utf-8");
    }
  }
  catch (e){
    console.log(e);
  }
  let json = JSON.parse(contents);
  let vcFromJson = -1;
  for(let i = 0; i < json.length; i++){
    if(json[i].members.includes(message.member.id)){
      if(vcFromJson !== -1){
        if(json[i].expiry >= json[vcFromJson].expiry){
          vcFromJson = i;
          continue;
        }
        continue;
      }
      vcFromJson = i;


    }
  }
  if(vcFromJson === -1){
    message.channel.send(`You were not found in a saved vc.`);
    return;
  }
  let actualVc = message.guild.channels.cache.get(json[vcFromJson].id);
  let RaidLeader = json[vcFromJson].RL;
  if(json[vcFromJson].expiry <= Date.now() || !actualVc.members.has(RaidLeader)){
    message.channel.send(`The vc you were found in has expired.`);
    return;
  }
  if(actualVc === message.member.voice.channel){
    message.channel.send("You are already in that channel.")
  }

  if (!actualVc.permissionsFor(message.member).has("CONNECT")) {
    actualVc.updateOverwrite(message.member,{ 'CONNECT': true }).then(()=>{
      message.member.voice.setChannel(actualVc);
    }).then(()=>{
      setTimeout(()=>{
        actualVc.permissionOverwrites.get(message.member.id).delete();
      }, 1000)
    })
  } else {
    message.member.voice.setChannel(actualVc);
  }
  message.channel.send("Channel moved");
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: "rejoin",
  description: "Rejoins the voice of the RL in case of DC",
  usage: 'USAGE: !rejoin'
};
