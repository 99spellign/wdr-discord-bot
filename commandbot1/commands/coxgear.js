exports.run = function(client, message, args){
    const Jimp = require('jimp');

    const minimal_gear = ["helm_of_neitiznot", "amulet_of_fury", "abyssal_whip", "kraken_tentacle", "zamorak_chaps", "dragon_boots", "berserker_ring", "ahrims_robetop", "ahrims_robeskirt", "zamorak_dhide", "occult_necklace", "toxic_blowpipe", "bandos_godsword", "barrows_gloves", "rune_pickaxe", "toxic_trident", "dragon_sword", "rune_crossbow"]
    
    const line_width = 50;
    
    var upgrades = [];

    //Thanks to AJCKTA#8411 for these new priority values

    upgrades.push({
        add : "necklace_of_anguish",
        remove : "",
        priority : "1",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "tormented_bracelet",
        remove : "",
        priority : "3",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "amulet_of_torture",
        remove : "amulet_of_fury",
        priority : "8",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "dragon_hunter_lance",
        remove : "abyssal_whip,kraken_tentacle,dragon_sword",
        priority : "2", //3 --> 2
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "dragon_warhammer",
        remove : "bandos_godsword",
        priority : "6",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "twisted_bow",
        remove : "rune_crossbow",
        priority : "4",
        skip : "",
        include : ""
    })


    upgrades.push({
        add : "scythe_of_vitur,abyssal_dagger",
        remove : "dragon_hunter_lance,abyssal_dagger",
        priority : "5",
        skip : "",
        include : "twisted_bow" //added dagger to scy/bow rebuild rebuild
    })

    upgrades.push({
        add : "dragon_hunter_crossbow",
        remove : "dragon_crossbow",
        priority : "7",
        skip : "twisted_bow",
        include : ""
    })

    upgrades.push({
        add : "sanguinesti_staff",
        remove : "toxic_trident",
        priority : "7",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "ferocious_gloves",
        remove : "",
        priority : "8", //7 --> 8
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "serpentine_helm",
        remove : "helm_of_neitiznot",
        priority : "8",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "dragon_pickaxe",
        remove : "rune_pickaxe",
        priority : "6",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "primordial_boots",
        remove : "dragon_boots",
        priority : "10",//8 --> 10
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "dragon_crossbow",
        remove : "rune_crossbow",
        priority : "6",
        skip : "twisted_bow,dragon_hunter_crossbow,armadyl_crossbow",
        include : ""
    })

    upgrades.push({
        add : "ancestral_hat",
        remove : "",
        priority : "8",//9 --> 8
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "armadyl_helmet",
        remove : "",
        priority : "9",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "pegasian_boots",
        remove : "",
        priority : "11",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "dragon_claws",
        remove : "",
        priority : "10",//11 --> 10
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "avernic_defender",
        remove : "",
        priority : "11",//12 --> 11
        skip : "",
        include : "scythe_of_vitur" //bow->scy, buy avernic after scy (assuming including scy will include bow)
    })

    upgrades.push({
        add : "ancestral_robe_top",
        remove : "ahrims_robetop",
        priority : "9",//10 --> 9
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "ancestral_robe_bottom",
        remove : "ahrims_robeskirt",
        priority : "9",//10 --> 9
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "armadyl_chestplate",
        remove : "zamorak_dhide",
        priority : "12",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "armadyl_chainskirt",
        remove : "zamorak_chaps",
        priority : "12",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "arcane_spirit_shield",
        remove : "",
        priority : "13",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "ghrazi_rapier",
        remove : "abyssal_dagger",
        priority : "13",
        skip : "",
        include : ""
    })


    upgrades.push({
        add : "bandos_tassets",
        remove : "",
        priority : "13",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "abyssal_dagger",
        remove : "dragon_sword",
        priority : "9",
        skip : "ghrazi_rapier,dragon_hunter_lance",
        include : ""
    })

    upgrades.push({
        add : "bandos_chestplate",
        remove : "",
        priority : "14",
        skip : "",
        include : ""
    })

    upgrades.push({
        add : "mages_book",
        remove : "",
        priority : "100",
        skip : "arcane_spirit_shield",
        include : ""
    })

    upgrades.push({
        add : "helm_of_neitiznot,basilisk_jaw",
        remove : "serpentine_helm",
        priority : "10",//11 --> 10
        skip : "",
        include : ""
    })


    const item_type = {
		toxic_trident : "mage",
		sanguinesti_staff : "mage",
		ahrims_robetop : "mage",
		ahrims_robeskirt : "mage",
		ancestral_robe_top : "mage",
		ancestral_robe_bottom : "mage",
		ancestral_hat : "mage",
		arcane_spirit_shield : "mage",
		occult_necklace : "mage",
		tormented_bracelet : "mage",
		imbued_saradomin_cape : "mage",
		mages_book : "mage",
		avas_assembler : "ranged",
		toxic_blowpipe : "ranged",
		rune_crossbow : "ranged",
		dragon_crossbow : "ranged", 
		armadyl_crossbow : "ranged",
		dragon_hunter_crossbow : "ranged",
		twisted_bow : "ranged",
		zamorak_chaps : "ranged",
		zamorak_dhide : "ranged",
		armadyl_chestplate : "ranged",
		armadyl_chainskirt : "ranged",
		armadyl_helmet : "ranged",
		pegasian_boots : "ranged",
		barrows_gloves : "ranged",
		necklace_of_anguish : "ranged",
		bandos_godsword : "melee",
		dragon_warhammer : "melee",
		scythe_of_vitur : "melee",
		dragon_claw : "melee"
    }

    upgrades.sort(function(a,b){
        if(parseInt(a.priority) < parseInt(b.priority)){
            return -1;
        }
        if(parseInt(a.priority) > parseInt(b.priority)){
            return 1
        }
        return 0;
    });

    var prices = {};

    const connection = client.connection;

	if(args.length == 0 || args[0] == undefined){
		message.channel.send("error: must specify a budget\nexample: !cox-gear 50m");
		return;
	}

    var money;
    if(args[0].substring(args[0].length-1, args[0].length).toLowerCase() == "k"){
        money = parseFloat(args[0].substring(0, args[0].length-1)) * 1000;
    }else if(args[0].substring(args[0].length-1, args[0].length).toLowerCase() == "m"){
        money = parseFloat(args[0].substring(0, args[0].length-1)) * 1000000;
    }else if(args[0].substring(args[0].length-1, args[0].length).toLowerCase() == "b"){
        money = parseFloat(args[0].substring(0, args[0].length-1)) * 1000000000;
    }else{
        money = parseFloat(args[0].substring(0, args[0].length));
    }

	if(money.toString() == "NaN"){
		message.channel.send("error: " + args[0] + " is not a number");
		return;
	}

    message.channel.send("budget : " + money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))

    var sql_command_1 = "SELECT * FROM WDR.GEAR_PRICE";
    connection.query(sql_command_1, async function(error, result, fields){
        if(error){ throw error }

        for(var i = 0; i < result.length; i++){
            prices[result[i].item] = result[i].price
        }

        var gear = JSON.parse(JSON.stringify(minimal_gear));

        if(price(minimal_gear) > money){
            message.channel.send("You are unable to afford the minimal gear set up");
            var message_string = format_prices(minimal_gear.sort())
            var message_chunks = chunkSubstr(message_string, 1620);
            for(var i = 0; i < message_chunks.length; i++){
                await message.channel.send("```" + message_chunks[i] + "```");
            }
            return
        }else{
            money -= price(gear);
        }

        for(var i = 0; i < upgrades.length; i++){
            if(money >= upgrade_cost(upgrades[i], prices) && (upgrades[i].skip == "" || !intersects(gear, upgrades[i].skip.split(","))) && (upgrades[i].include == "" || gear.includes(upgrades[i].include))){

                var add = upgrades[i].add.split(",");
                for(var j = 0; j < add.length; j++){
                    gear.push(add[j]);
                }

                var remove = upgrades[i].remove.split(",");
                for(var j = 0; j < remove.length; j++){
                    var found = gear.indexOf(remove[j]);
                    if (found != -1) {
                        gear.splice(found, 1);
                    }
                }
                money -= upgrade_cost(upgrades[i], prices);
            }
        }

        var message_string = format_prices(gear.sort());
        var message_chunks = chunkSubstr(message_string, 1989);
        for(var i = 0; i < message_chunks.length; i++){
            await message.channel.send("```" + message_chunks[i] + "```");
        }
        gear_pic(message, gear);
    });
    
    function price(items){
        var price = 0;

        for(var i = 0; i < items.length; i++){
            price += prices[items[i]];
        }
        return price;
    }

    function intersects(a, b){
        for(var i = 0; i < a.length; i++){
            if(b.includes(a[i])){
                return true;
            }
        }
        return false;
    }

    function upgrade_cost(upgrade, item_prices){
        var cost = 0;
        var add = upgrade.add.split(",");
        for(var i = 0; i < add.length; i++){
            cost += item_prices[add[i]];
        }

        var removed = [];
        if(upgrade.remove != ""){
            removed = upgrade.remove.split(",");
        }
            
        for(var i = 0; i < removed.length; i++){
            cost -= item_prices[removed[i]];
        }

        return cost;
    }

    function format_prices(gear){

        var message_string = "";
        var price = 0;
        
        for(var i = 0; i < gear.length; i++){
            price += prices[gear[i]];
        }

        var longest_name = 5; //total
        for(var i = 0; i < gear.length; i++){
            if(gear[i].length > longest_name){
                longest_name = gear[i].length;
            }

        }

        var longest_price = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").length;

        for(var i = 0; i < gear.length; i++){
            message_string += back_fill_with_spaces(gear[i].replace(/_/g,' '), longest_name) + spaces(line_width - longest_name - longest_price ) + front_fill_with_spaces(prices[gear[i]].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","), longest_price) + "\n";
        }
        message_string += repeat_character("-", line_width) + "\n";
        message_string += back_fill_with_spaces("total", longest_name) + spaces(line_width -longest_name - longest_price ) + price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return message_string;

    }

    async function gear_pic(message, gear){
        var setup = organize_gear(JSON.parse(JSON.stringify(gear)))

        await Jimp.read("/root/wdrbot/gear/background.png", async function(error, image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.off_hand + ".png", async function(error, off_hand_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.main_hand + ".png", async function(error, main_hand_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.head + ".png", async function(error, head_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.cape + ".png", async function(error, cape_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.ring + ".png", async function(error, ring_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.boots + ".png", async function(error, boots_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.body + ".png", async function(error, body_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.legs + ".png", async function(error, legs_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.gloves + ".png", async function(error, gloves_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.neck + ".png", async function(error, neck_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.ammo + ".png", async function(error, ammo_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[0] + ".png", async function(error, inv0_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[1] + ".png", async function(error, inv1_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[2] + ".png", async function(error, inv2_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[3] + ".png", async function(error, inv3_image){
            if(error) { throw error }
         await Jimp.read("gear/" + setup.other[4] + ".png", async function(error, inv4_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[5] + ".png", async function(error, inv5_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[6] + ".png", async function(error, inv6_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[7] + ".png", async function(error, inv7_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[8] + ".png", async function(error, inv8_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[9] + ".png", async function(error, inv9_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[10] + ".png", async function(error, inv10_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[11] + ".png", async function(error, inv11_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[12] + ".png", async function(error, inv12_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[13] + ".png", async function(error, inv13_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[14] + ".png", async function(error, inv14_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[15] + ".png", async function(error, inv15_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[16] + ".png", async function(error, inv16_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[17] + ".png", async function(error, inv17_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[18] + ".png", async function(error, inv18_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[19] + ".png", async function(error, inv19_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[20] + ".png", async function(error, inv20_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[21] + ".png", async function(error, inv21_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[22] + ".png", async function(error, inv22_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[23] + ".png", async function(error, inv23_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[24] + ".png", async function(error, inv24_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[25] + ".png", async function(error, inv25_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[26] + ".png", async function(error, inv26_image){
            if(error) { throw error }
        await Jimp.read("gear/" + setup.other[27] + ".png", async function(error, inv27_image){
            if(error) { throw error }


            image
                .composite(off_hand_image.resize(50, 50), 180, 155)
                .composite(ring_image.resize(50,50), 180, 280)
                .composite(body_image.resize(50, 50), 95, 155)
                .composite(main_hand_image.resize(50, 50), 5, 150)
                .composite(head_image.resize(50, 50), 95, 40)
                .composite(cape_image.resize(50, 50), 30, 95)
                .composite(boots_image.resize(50,50), 95, 275)
                .composite(legs_image.resize(50,50), 95, 215)
                .composite(gloves_image.resize(50, 50), 5, 275)
                .composite(neck_image.resize(50, 50), 95, 95)
                .composite(ammo_image.resize(50, 50), 155, 95)
                .composite(inv0_image.resize(35, 35), 265, 40)
                .composite(inv1_image.resize(35, 35), 313, 40)
                .composite(inv2_image.resize(35, 35), 361, 40)
                .composite(inv3_image.resize(35, 35), 409, 40)
                .composite(inv4_image.resize(35, 35), 265, 77)
                .composite(inv5_image.resize(35, 35), 313, 77)
                .composite(inv6_image.resize(35, 35), 361, 77)
                .composite(inv7_image.resize(35, 35), 409, 77)
                .composite(inv8_image.resize(35, 35), 265, 114)
                .composite(inv9_image.resize(35, 35), 313, 114)
                .composite(inv10_image.resize(35, 35), 361, 114)
                .composite(inv11_image.resize(35, 35), 409, 114)
                .composite(inv12_image.resize(35, 35), 265, 151)
                .composite(inv13_image.resize(35, 35), 313, 151)
                .composite(inv14_image.resize(35, 35), 361, 151)
                .composite(inv15_image.resize(35, 35), 409, 151)
                .composite(inv16_image.resize(35, 35), 265, 188)
                .composite(inv17_image.resize(35, 35), 313, 188)
                .composite(inv18_image.resize(35, 35), 361, 188)
                .composite(inv19_image.resize(35, 35), 409, 188)
                .composite(inv20_image.resize(35, 35), 265, 225)
                .composite(inv21_image.resize(35, 35), 313, 225)
                .composite(inv22_image.resize(35, 35), 361, 225)
                .composite(inv23_image.resize(35, 35), 409, 225)
                .composite(inv24_image.resize(35, 35), 265, 262)
                .composite(inv25_image.resize(35, 35), 313, 262)
                .composite(inv26_image.resize(35, 35), 361, 262)
                .composite(inv27_image.resize(35, 35), 409, 262)
                .write("output.png")
            message.channel.send('Suggested Gear Setup', { files: ["./output.png"] });

        })//inv27
        })//inv26
        })//inv25
        })//inv24
        })//inv23
        })//inv22
        })//inv21
        })//inv20
        })//inv19
        })//inv18
        })//inv17
        })//inv16
        })//inv15
        })//inv14
        })//inv13
        })//inv12
        })//inv11
        })//inv10
        })//inv9    
        })//inv8
        })//inv7
        })//inv6
        })//inv5
        })//inv4
        })//inv3
        })//inv2
        })//inv1
        })//inv0
        })//ammo
        })//neck
        })//gloves  
        })//legs
        })//body
        })//boots
        })//ring
        })//cape
        })//head
        })//main-hand
        })//off-hand
        })//background

        
    }

    function organize_gear(gear){
        var setup = {
            neck : "",
            body : "",
            legs : "",
            boots : "",
            ring : "berserker_ring",
            head : "",
            cape : "infernal_cape",
            main_hand : "", 
            off_hand : "",
            gloves : "",
            ammo : "",
            other : []
        }

        if(gear.includes("twisted_bow")){
            setup.ammo = "dragon_arrow";
        }else if(gear.includes("dragon_crossbow") || gear.includes("armadyl_crossbow") || gear.includes("dragon_hunter_crossbow")){
            setup.ammo = "ruby_dragon_bolt(e)";
        }else if(gear.includes("rune_crossbow")){
            setup.ammo = "ruby_bolt(e)";
        }

        if(gear.includes("amulet_of_torture")){
            setup.neck = "amulet_of_torture";
            remove(gear, "amulet_of_torture");
        }else if(gear.includes("amulet_of_fury")){
            setup.neck = "amulet_of_fury";
            remove(gear, "amulet_of_fury");
        }

        if(gear.includes("ferocious_gloves")){
            setup.gloves = "ferocious_gloves";
            remove(gear, "ferocious_gloves");
        }else if(gear.includes("barrows_gloves")){
            setup.gloves = "barrows_gloves";
            remove(gear, "barrows_gloves");
        }

        if(gear.includes("bandos_tassets")){
            setup.legs = "bandos_tassets";
            remove(gear, "bandos_tassets");
        }else if(gear.includes("ancestral_robe_bottom")){
            setup.legs = "ancestral_robe_bottom";
            remove(gear, "ancestral_robe_bottom");
        }else if(gear.includes("ahrims_robeskirt")){
            setup.legs = "ahrims_robeskirt";
            remove(gear, "ahrims_robeskirt");
        }

        if(gear.includes("bandos_chestplate")){
            setup.body = "bandos_chestplate";
            remove(gear, "bandos_chestplate");
        }else{
            setup.body = "fighter_torso";
        }

        if(gear.includes("primordial_boots")){
            setup.boots = "primordial_boots";
            remove(gear, "primordial_boots");
        }else if(gear.includes("dragon_boots")){
            setup.boots = "dragon_boots";
            remove(gear, "dragon_boots")
        }

        remove(gear, "berserker_ring")

		if(gear.includes("basilisk_jaw")){
			setup.head = "neitiznot_faceguard";
			remove(gear, "helm_of_neitiznot");
			remove(gear, "basilisk_jaw");
		}else if(gear.includes("serpentine_helm")){
            setup.head = "serpentine_helm";
            remove(gear, "serpentine_helm");
        }else if(gear.includes("helm_of_neitiznot")){
            setup.head = "helm_of_neitiznot";
            remove(gear, "helm_of_neitiznot")
        }

        if(gear.includes("avernic_defender")){
            setup.off_hand = "avernic_defender";
            remove(gear, "avernic_defender");
        }else{
            setup.off_hand = "dragon_defender";
        }

        if(gear.includes("abyssal_whip")){
            setup.main_hand = "abyssal_whip"
            remove(gear,"abyssal_whip")
            remove(gear, "kraken_tentacle")
        }else if(gear.includes("dragon_hunter_lance")){
            setup.main_hand = "dragon_hunter_lance";
            remove(gear, "dragon_hunter_lance");
        }else if(gear.includes("dragon_warhammer")){
            setup.main_hand = "dragon_warhammer";
            remove(gear, "dragon_warhammer");
        }


        if(gear.includes("augury")){
            remove(gear, "augury");
        }

        if(gear.includes("rigour")){
            remove(gear, "rigour");
        }


        //if(gear.includes("dragon_pickaxe")){
        //	remove(gear, "dragon_pickaxe");
        //}

		//if(gear.includes("rune_pickaxe")){
		//	remove(gear, "rune_pickaxe");
		//}

      	//if(gear.includes("dragon_sword")){
       	//    remove(gear, "dragon_sword");
       	//}

	   	//if(gear.includes("abyssal_dagger")){
		//	remove(gear, "abyssal_dagger")
		//}

        //if(gear.includes("ghrazi_rapier")){
        //    remove(gear, "ghrazi_rapier");
        //}

        if(gear.includes("abyssal_bludgeon")){
            remove(gear, "abyssal_bludgeon");
        }

        gear.push("imbued_saradomin_cape");
        gear.push("avas_assembler");

        //gear.push("anglerfish");
        //gear.push("saradomin_brew");
        //gear.push("saradomin_brew");
        //gear.push("super_restore");
        //gear.push("super_restore");
        //gear.push("bastion_potion");
        //gear.push("super_combat_potion");
        //gear.push("stamina_potion");
		
		gear.sort(function(a,b){
			var a_type = item_type[a];
			var b_type = item_type[b];

			if(a_type == undefined){
				a_type = "z-other";
			}
			if(b_type == undefined){
				b_type = "z-other";
			}

			if(a_type < b_type){
				return -1;
			}else if(a_type > b_type){
				return 1;
			}else{
				return 0;
			}
		})

        while(gear.length < 28){
            gear.push("blank");
        }



        setup.other = gear;
        return setup;

    }

    function remove(arr, what) {
        var found = arr.indexOf(what);

        while (found !== -1) {
            arr.splice(found, 1);
            found = arr.indexOf(what);
        }
    }


    function chunkSubstr(str, size) {
        const numChunks = Math.ceil(str.length / size)
        const chunks = new Array(numChunks)

        for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
            chunks[i] = str.substr(o, size)
        }

        return chunks
    }

    function repeat_character(character, amount){
        var ret = "";
        for(var i = 0; i < amount; i++){
            ret += character;
        }
        return ret;
    }

    function spaces(amount){
        var ret = "";
        for(var i = 0; i < amount; i++){
            ret += " ";
        }
        return ret;
    }
    function front_fill_with_spaces(str, length){
        while(str.length < length){
            str = " " + str;
        }
        return str;
    }

    function back_fill_with_spaces(str, length){
        while(str.length < length){
            str += " ";
        }
        return str;
    } 
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'cox-gear',
  description: 'gives cox gear for budget',
  usage: 'cox-gear amount'
};
