exports.run = async function(client, message, args){
	const Jimp = require('jimp');
	const Filter = require('bad-words');

	var filter = new Filter();

    var text = args.join(" ");
	text = filter.clean(text);

    await Jimp.read("files/worry.png", async function(error, image){
      if(error) { throw error }

      Jimp.loadFont("files/font.fnt").then(font => {
        image.resize(256,256).print(font, 0, 0,{
            text: text,
            alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
            alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
          }, 256, 256).write("worrytext.png");
        message.channel.send('', { files: ["./worrytext.png"]});
      });
    });
    
    return;
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.EVERYONE
};

exports.help = {
  name: 'worry',
  description: 'worry',
  usage: 'worry'
};
