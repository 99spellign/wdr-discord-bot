exports.run = async function(client, message){
  await message.channel.send("Getting mentor activity log from the last month");
  await message.guild.members.fetch();
  let connection = client.connection;
  let mentors = keys(message.guild.roles.cache.get("408107637100511243").members);
  let mentorRole = "408107637100511243";
  let coxMentor = "520749698424766475";
  let tobMentor = "520750027405262858";
  let fromSql = new Map()

  let coxQuotaFail = new Array();
  let tobQuotaFail = new Array();
  let totalQuotaFail = new Array();
  let quotaReached = new Array();

  //get all users of last month
  let sql_command = "SELECT user_id, message, count(*) AS amount FROM WDR.MENTOR_RAID_LOGS WHERE YEAR(msg_time ) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(msg_time ) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) GROUP BY user_id, message"

  await connection.query(sql_command, async function(error, result, fields){
    if(error) { throw error }
    if(result != undefined && result != null){
      data = result;
      if(data.length == 0){
        message.channel.send("Noone met quota.");
        return;
      }
      for(let i = 0; i < data.length; i++){
        if(!fromSql.has(data[i].user_id)){
          fromSql.set(data[i].user_id, {[data[i].message]: data[i].amount});
          continue;
        }
        let mapData = fromSql.get(data[i].user_id);
        mapData[data[i].message] = data[i].amount;
        fromSql.set(data[i].user_id, mapData);
      }


      mentors.forEach(el => {
        el = message.guild.members.cache.get(el);
        if(el.roles.cache.has(coxMentor) && el.roles.cache.has(tobMentor)){
          let userData = fromSql.get(el.id);
          if(userData === undefined){
            coxQuotaFail.push("<@!"+el.id+">");
          }
          else if(userData["cox"] < 1){
            coxQuotaFail.push("<@!"+el.id+">");
          }
          if(userData === undefined) {
            tobQuotaFail.push("<@!" + el.id + ">");
          }
          else if(userData["tob"] < 1 && userData["tob trial"] < 1){
            tobQuotaFail.push("<@!"+el.id+">");
          }
        }
        else if(el.roles.cache.has(coxMentor)){
          let userData = fromSql.get(el.id);
          if(userData === undefined){
            coxQuotaFail.push("<@!"+el.id+">");
          }
          else if(userData["cox"] < 2){
            coxQuotaFail.push("<@!"+el.id+">");
          }
        }
        let userData = fromSql.get(el.id);
        if(userData === undefined){
          userData = {"cox": 0}
        }
        if(sum(userData) < 4){
          totalQuotaFail.push("<@!"+el.id+">");
        }


      });

      fromSql.forEach( (el, key) =>{
        let user = message.guild.members.resolve(key.toString());
        if(user == null){
          return;
        }
        if(user.roles.cache.has(mentorRole))
          return;
        if(el["cox"] >= 4){
          quotaReached.push("<@!"+key+"> (CoX)");
        }
        if(el["tob"]+el["tob trial"] >= 4){
          quotaReached.push("<@!"+key+"> (ToB)");
        }
        if(el["cox"]+el["tob"] >= 4 && el["cox"] >= 2 && el["tob"] >= 2){
          quotaReached.push("<@!"+key+"> (Both)")
        }

      })

      let msg = "**Mentor activity check:**\n";

      msg += "Tob quota fails:\n";
      msg += tobQuotaFail.join("\n");
      msg += "\n\nCox quota fails:\n";
      msg += coxQuotaFail.join("\n");
      msg += "\n\nQuota fails:\n";
      msg += totalQuotaFail.join("\n");
      msg += "\n\nQuota reached but not active mentor:\n"
      msg += quotaReached.join("\n");
      msg += "\n\nThis command is still in it's early stages, please double check if I missed anyone.";

      message.channel.send(msg);

    }


  });

  function sum(obj) {
    return Object.keys(obj).reduce((sum,key)=>sum+parseFloat(obj[key]||0),0);
  }
  function keys(arg){
    var k = new Set();
    for(var [key, value] of arg){
      k.add(key)
    }
    return k;
  }
}


exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["mac"],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: "mentor-activity-check",
  description: "Gets all active mentors who didn't meet quota",
  usage: "mentor-activity-check"
};
