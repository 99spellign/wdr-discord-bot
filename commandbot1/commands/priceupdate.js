exports.run = async function(client, message, args){

	const fetch = require("node-fetch");

	const url = "https://prices.runescape.wiki/api/v1/osrs/latest?id="
	const price_urls = {
		rune_pickaxe : "1275",
		twisted_bow : "20997",
		helm_of_neitiznot : "10828",
		amulet_of_fury : "6585",
		abyssal_whip : "4151",
		kraken_tentacle : "12004",
		zamorak_chaps : "10372",
		dragon_boots : "11840", 
		berserker_ring : "6737",
		ahrims_robetop : "4712",
		ahrims_robeskirt : "4714",
		zamorak_dhide : "10370",
		zamorak_dhide_boots : "19936",
		occult_necklace : "12002",
		toxic_blowpipe : "12924",
		toxic_trident : "12900",
		bandos_godsword : "11804",
		dragon_pickaxe : "11920",
		necklace_of_anguish : "19547",
		rigour : "21034",
		tormented_bracelet : "19544",
		amulet_of_torture : "19553",
		dragon_hunter_lance : "22978",
		dragon_warhammer : "13576", 
		scythe_of_vitur : "22486",
		dragon_crossbow : "21902",
		armadyl_crossbow : "11785", 
		dragon_hunter_crossbow : "21012",
		sanguinesti_staff : "22481",
		ferocious_gloves : "22983",
		serpentine_helm : "12929",
		primordial_boots : "13239",
		augury : "21079",
		ancestral_hat : "21018", 
		ancestral_robe_bottom : "21024",
		ancestral_robe_top : "21021",
		armadyl_chainskirt : "11830", 
		armadyl_chestplate : "11828",
		armadyl_helmet : "11826",
		pegasian_boots : "13237",
		avernic_defender : "22477",
		arcane_spirit_shield : "12825",
		ghrazi_rapier : "22324",
		abyssal_bludgeon : "13263",
		bandos_tassets : "11834",
		bandos_chestplate : "11832",
		rune_crossbow : "9185",
		dragon_sword : "21009",
		abyssal_dagger : "13265",
		basilisk_jaw : "24268", 
		mages_book : "6889",
		dragon_claws : "13652"
	}

	const connection = client.connection;

	console.log("updating prices");

	var sql_command_3 = "SELECT * FROM WDR.GEAR_PRICE";
	var skips = [];
	connection.query(sql_command_3, function(error, result, fields){
		if(error){ throw error }
		if(result != undefined && result != null && result.length != 0){
			for(var i = 0; i < result.length; i++){
				skips.push(result[i].item);
			}
		}
	})
	  for (var key in price_urls){
      var id = price_urls[key];
        var response = await fetch(url + id, {
          headers:{
            'User-Agent': 'price-update discord.gg/wdr'
          }
        })
          .then(response => response.json())
          .then(resp => {
            /*console.log(resp);
            console.log(id);
            console.log(resp.data[id].high);*/
            value = resp.data[id].high;
            if(value != ""){
              var sql_command_1 = "DELETE FROM WDR.GEAR_PRICE WHERE item =\'" + key + "\'";
              if(args[0] == "-debug"){
                message.channel.send(sql_command_1);
              }
              connection.query(sql_command_1, async function(error, result, fields){
                if(error){ throw error }
              });
      
              var sql_command_2 = "INSERT INTO WDR.GEAR_PRICE (item, price, last_update_time) VALUES (\"" + key + "\", " + value +  ",\'" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "\');";
              if(args[0] == "-debug"){
                message.channel.send(sql_command_2);
              }
              connection.query(sql_command_2, async function(error, result, fields){
                if(error){ throw error }
              });  
            }


          });
    }
	message.channel.send("done");
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.MOD
};

exports.help = {
  name: 'price-update',
  description: 'fetchs the most recent gear prices',
  usage: '!price-update'
};
