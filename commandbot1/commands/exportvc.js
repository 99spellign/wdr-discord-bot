const fs = require('fs');
exports.run = function(client, message) {
  var output_string = "";

  // User must be in channel
  if (!message.member.voice.channel) {
    message.channel.send(`You must be in the voice channel to use this command`);
    return;
  }

  let vc = message.member.voice.channel;

  let members = vc.members.array();

  for (let i = 0; i < members.length; i++) {
    var user = members[i];
    var name = user.nickname;
    console.log(name);
    if (name == null || name == undefined) {//use discord name if no sever nickname
      name = members[i].user.username;
    }
    var userNames = new Array();
    if (name.includes("(")) {
      name = name.split('(')[0]
    }
    if (name.includes("[")) {
      name = name.split('[')[0]
    }
    if (name.includes("|")) {
      userNames = name.split('|');
    } else {
      userNames = [name];
    }
    for (let rsn of userNames) {
      rsn = rsn.trim().toLowerCase();
      if(rsn.length == 0)
        continue;
      output_string += rsn + "\n";
    }
  }

  fs.writeFile('vc-export.txt', output_string , (err) => {
    if (err) { throw err }
  })

  message.channel.send("", { files: ["./vc-export.txt"] });

}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["evc"],
  permLevel: permLevel.INACTIVEMENTOR
};

exports.help = {
  name: "export-vc",
  description: "Exports a list of rsns of people in the voice channel",
  usage: 'USAGE: !export-vc'
};
