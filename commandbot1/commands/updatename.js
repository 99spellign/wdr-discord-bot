var time = new Date(0);
exports.run = async function(client, message, args) {
  const {google} = require("googleapis"); //npm install googleapis@39 --save
  const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well
  const Discord = require("discord.js");
  const banlistUpdate = require ("../util/banlistupdate.js");
  const toJagexName = require ("../util/toJagexName.js");
  var rsns = args.join(" ").trim();
  const fetch = require("node-fetch");
  const connection = client.connection;

  var rsnArr = rsns.split("|");
  var rsn1 = rsnArr[0].trim();
  var rsn2 = rsnArr[1].trim();
  rsn2 = toJagexName(rsn2);
  if(rsn1.length == 0 || rsn2.length == 0 || rsn1 == undefined || rsn2 == undefined){
    message.channel.send("Invalid Usage");
    return;
  }
  const google_client = new google.auth.JWT(
    google_sheet_keys.client_email,
    null,
    google_sheet_keys.private_key,
    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
  );

  google_client.authorize(function (error, tokens) {
    if (error) {
      console.log(error);
      return;
    }
  });


  const google_sheet = google.sheets({version: "v4", auth: google_client})
  const opt = {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Data"
  }
  var data = await google_sheet.spreadsheets.values.get(opt);
  var banlist = data.data.values;
  let match=false;
  for (var i = 1; i < banlist.length; i++) {
    if(banlist[i][0] != undefined && banlist[i][0].trim().toUpperCase() == rsn1.trim().toUpperCase()){
      match = true;
      match_row = [];
      match_row[0] = banlist[i];
      match_row[0][2] = banlist[i][0]+", "+match_row[0][2];
      match_row[0][0] = rsn2;
      match_row[0][7] = "TRUE";
      rownumber = i + 1;
      const opt = {
        spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
        range: "Data!" + rownumber + ":" + rownumber,
        valueInputOption: "USER_ENTERED",
        resource: {values: match_row}
      };
      var res = await google_sheet.spreadsheets.values.update(opt);
      if(res.status === 200){
        message.channel.send("Successfully updated rsn `"+rsn1+"` to `"+rsn2+"`.");
        banlistUpdate(client);
      }
      else{
        message.channel.send("Something went wrong.");
      }
    }

  }
  if(!match){
      message.channel.send(rsn1+" not found on the banlist");
      return;
    }
  //Getting stats, should probably be put in util but w/e, will fix one day
  var name = rsn2;
  var lookup = await getStats(name);
  if(lookup.response == 200){
    var sql_command_4 = "DELETE FROM WDR.USER_STATS WHERE rsn=\'" + name + "\'";
    await connection.query(sql_command_4, function(error, result, fields){
      if(error){ throw error }
    })
    var sql_command_5 = "INSERT INTO WDR.USER_STATS (rsn, stats, last_update) VALUES (\'" + name + "\',\'" + JSON.stringify(lookup.data) + "\',\'" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "\')";
    await connection.query(sql_command_5, function(error, result, fields){
      if(error){ throw error }
    })
    message.channel.send("data saved");
  }else{
    message.channel.send("rsn not found on hiscore. Response code: "+lookup.response);
  }




  if(time.getTime() + 86400000 < Date.now()) {
    const banlistChannelUpdate = require("../util/banlistchannelupdate.js");
    banlistChannelUpdate(client);
    time = new Date();
  }

  async function getStats(rsn) {
    const response = await fetch("https://secure.runescape.com/m=hiscore_oldschool/a=97/index_lite.ws?player=" + encodeURIComponent(rsn), {"redirect": "manual"});
    const text = await response.text();
    if(response.status == 200){
      return ({"response":response.status, "data": parseStatsData(text)})
    }else{
      return({"response":response.status, "data":null});
    }
  }

  function parseStatsData (data) {
    const skills = ['overall','attack','defence','strength','hitpoints','ranged','prayer','magic','cooking','woodcutting','fletching','fishing','firemaking','crafting','smithing','mining','herblore','agility','thieving','slayer','farming','runecraft','hunter','construction'];
    var sortedData = {};
    var splitData = data.split('\n');
    for (var i = 0; i < skills.length; i++) {
      var level = splitData[i].split(',')[1];
      var exp = splitData[i].split(',')[2];
      sortedData[skills[i]] = {"level" : level, "exp" : exp};
    }
    return sortedData;
  }

};
exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.MOD
};

exports.help = {
  name: 'updatename',
  description: 'changes a name of a player in the banlist sheet',
  usage: 'updatename `rsn1` | `rsn2`'
};