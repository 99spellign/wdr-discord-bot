exports.run = async function(client, message, args) {

  if(message.channel.id == 417638357959573505) { //mod chat
    message.channel.send("You can't execute that command here.")
    return;
  }
  let memberMap = new Map();
  var members = message.guild.members.cache;
  for(var member of members) {
    try{
      var user = await message.guild.members.fetch(member[1].user.id);
    }catch(e){
      console.log(member[1].user.id+ " gave error "+e);
    }
    let roles = user.roles.cache;
    if (args[0] == "--filter") {
      if (roles.has("408221074833145863")) {
        continue;
      }
      if(roles.has("800461880941346846")){
        continue;
      }
      if(roles.has("804051618312093727")){
        continue;
      }
      if(roles.has("442590109599268864")){
        continue;
      }
      if(roles.size == 1){
        continue;
      }
    }

    var user = message.guild.members.cache.get(member[1].user.id);
    var name = user.nickname;
    if (name == null || name == undefined) {//use discord name if no sever nickname
      name = member[1].user.username;
    }
    var userNames = new Array();
    if(name.includes("(")){
      name = name.split('(')[0]
    }
    if(name.includes("[")){
      name = name.split('[')[0]
    }
    if(name.includes("|")){
      userNames = name.split('|');
    }
    else{
      userNames = [name];
    }
    for(let rsn of userNames) {
      rsn = rsn.trim().toLowerCase();
      if(rsn.length == 0)
        continue;
      // console.log(rsn);
      if (memberMap.has(rsn)) {
        memberMap.get(rsn).occurences++
        memberMap.get(rsn).userId += ","+member[1].user.id;
      }
      else{
        memberMap.set(rsn, {occurences: 1, userId: member[1].user.id})
      }
    }
  }
  let mapFiltered = new Map([...memberMap].filter(([k, v]) => v.occurences > 1));

  message.channel.send("Dupe names found: "+mapFiltered.size);

  for(var dupeuser of mapFiltered.values()){
    userids = dupeuser.userId.split(',');
    if(args[0] == "--filter"){
      userids.map(async el => {
        let memberToAddRole = message.guild.members.resolve(el);
        let roles = ['800461880941346846'];
        if(memberToAddRole.roles.cache.has("408247451217166336"))
          roles.push("408247451217166336");
        if(memberToAddRole.roles.cache.has("408247454207705088"))
          roles.push("408247454207705088");
        if(memberToAddRole.roles.cache.has("637279237841354754") || memberToAddRole.roles.cache.has("423653735449886720")  || memberToAddRole.roles.cache.has("408106863117336577"))
          return;
        try {
          await memberToAddRole.roles.set(roles);
        }
        catch(err){
          message.channel.send("FAILED TO ADD ROLE TO <@!"+memberToAddRole.id+"> "+memberToAddRole.id)
        }
      });
    }
    var pings = userids.map(el => "<@!"+el+">");
    message.channel.send(pings.join(" "));
  }
  message.channel.send("done.");

}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: permLevel.TRIALMOD
};

exports.help = {
  name: 'dupe-names',
  description: 'dupe-names',
  usage: 'dupe-names'
};
