/**
 * Convert a string into Jagex username format, the same way runelite does
 * Remove all non-ascii characters, replace nbsp with space, replace _- with spaces, and trim
 *
 * @param {string} str - The string to standardize
 * @return {string} - The given `str` that is in Jagex name format
 */
//THANKS RUNELITE
module.exports = function(str) {
  var chars = str.split('');
  var newIdx = 0;
  for (var oldIdx = 0, strLen = str.length; oldIdx < strLen; oldIdx++){
    var c = chars[oldIdx];
    // take care of replacing and trimming in 1 go
    if (c === '\u00A0' || c === '-' || c === '_' || c === ' ')
    {
      if (oldIdx === strLen - 1 || newIdx === 0 || chars[newIdx - 1] === ' ')
      {
        continue;
      }
      c = ' ';
    }
    // 0 - 127 is valid ascii
    if (c > 127)
    {
      continue;
    }
    chars[newIdx++] = c;
  }

  return chars.join('').substr(0, newIdx);
}



