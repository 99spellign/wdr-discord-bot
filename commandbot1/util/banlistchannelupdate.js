module.exports = async function(client) {

  const {google} = require("googleapis"); //npm install googleapis@39 --save
  const google_sheet_keys = require("/root/wdrbot/google_sheet_keys.json"); //top secret do not share, file path may need to be adjusted as well
  const Discord = require("discord.js");

  var Banlist_Column_Headers = [];

  const google_client = new google.auth.JWT(
    google_sheet_keys.client_email,
    null,
    google_sheet_keys.private_key,
    ["https://www.googleapis.com/auth/spreadsheets"] //read write permissions
  );

  google_client.authorize(function (error, tokens) {
    if (error) {
      console.log(error);
      return;
    }
  });

  const google_sheet = google.sheets({version: "v4", auth: google_client})
  const opt = {
    spreadsheetId: "1RCSAGdCyef2h2whTga6Q5uCAyEh3ffU9WOKmaryl2gs",
    range: "Data"
  }

  var data = await google_sheet.spreadsheets.values.get(opt);
  var banlist = data.data.values;
  var sort = {};
  sort["Scammer (Twisted Bow)"] = [];
  sort["Scammer"] = [];
  sort["Repaid"] = [];
  sort["Rule"] = [];
  for (var i = 1; i < banlist.length; i++) {
    if(banlist[i][8] == undefined)
      continue;
    if (banlist[i][8] == "Scammer (Twisted Bow)") {
      sort["Scammer (Twisted Bow)"].push(banlist[i][0])
    } else if (banlist[i][8].substring(0, 7) == "Scammer") {
      sort["Scammer"].push(banlist[i][0])
    } else if (banlist[i][8].substring(0, 4) == "Rule") {
      sort["Rule"].push(banlist[i][0])
    }
  }

  var channel = client.channels.cache.get('718756105798549544');

  const rows_per_message = 20;
  var msg = "";
  channel.messages.fetch({
    limit: 100
  }).then(messages => {
    for (var m of messages) {
      m[1].delete();
    }
  })
    .then(() => channel.send("", {files: ["https://cdn.discordapp.com/attachments/482143936664567809/482144728872386561/asds.png"]}))
    .then(() => channel.send("", {files: ["https://cdn.discordapp.com/attachments/482143936664567809/482145077549072385/asdas.png"]}))
    .then(() => channel.send("**We highly recommend you install the __Runewatch__ plugin from the Runelite plugin hub.**\n" +
      "Since there are over 400 banned users in this list, you can no longer rely on your ignore list to prevent scammers. The plugin will notify you any time someone on the WDR ban list or Runewatch tries to join your raid or cc. This prevents scammers making alts and continuing to sneak into raids with people from this server.\n" +
      "We make announcements occasionally for persistent scammers who are trying to enter raids, so blocking them will prevent them from ever being able to join a raid hosted by you. Aside from that, it is much more effective to just use the plugin."))
    .then(() => channel.send("", {files: ["https://cdn.discordapp.com/attachments/482143936664567809/482145077549072385/asdas.png"]}))
    .then(() => channel.send(":x:__Twisted Bow__:x:"))
    .then(() => {
      for (var i = 0; i < sort["Scammer (Twisted Bow)"].length; i++) {

        msg += '<:cox:651156619824070687>`' + sort["Scammer (Twisted Bow)"][i] + "`";
        if (i % rows_per_message != rows_per_message - 1) {
          msg += "\n";
        }
        if (i % rows_per_message == rows_per_message - 1 || i == sort["Scammer (Twisted Bow)"].length - 1) {
          channel.send(msg);
          msg = "";
        }
      }
    })
    .then(() => channel.send(":x:__Other scams__:x:"))
    .then(() => {
      for (var i = 0; i < sort["Scammer"].length; i++) {

        msg += '<:cox:651156619824070687>`' + sort["Scammer"][i] + "`";
        if (i % rows_per_message != rows_per_message - 1) {
          msg += "\n";
        }
        if (i % rows_per_message == rows_per_message - 1 || i == sort["Scammer"].length - 1) {
          channel.send(msg);
          msg = "";
        }
      }
    })
    .then(() => channel.send(":x:__Rule Breaking__:x:"))
    .then(() => {
      for (var i = 0; i < sort["Rule"].length; i++) {

        msg += '<:cox:651156619824070687>`' + sort["Rule"][i] + "`";
        if (i % rows_per_message != rows_per_message - 1) {
          msg += "\n";
        }
        if (i % rows_per_message == rows_per_message - 1 || i == sort["Rule"].length - 1) {
          channel.send(msg);
          msg = "";
        }
      }
    })
    .then(() => {
      var embed = new Discord.MessageEmbed().setColor(293680)
      embed.setTitle("__**Last Updated**__");
      embed.setTimestamp(new Date());
      embed.setFooter("WDR Bot");
      channel.send(embed);
    });
}