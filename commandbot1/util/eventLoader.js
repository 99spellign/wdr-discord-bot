const reqEvent = (event) => require(`../events/${event}`);
module.exports = client => {
  client.on('ready', () => reqEvent('ready')(client));
  client.on('message',reqEvent('message'));
  client.on('guildMemberAdd',reqEvent('guildMemberAdd'));
  client.on('guildMemberRemove',reqEvent('guildMemberRemove'));
  client.on('messageUpdate',reqEvent('messageUpdate'))
  client.on('messageReactionAdd',reqEvent('messageReactionAdd'))
  client.on('messageReactionRemove',reqEvent('messageReactionRemove'))
};
