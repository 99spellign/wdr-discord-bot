const Discord = require("discord.js");

module.exports = async member => {
  if(member.partial) await member.fetch();
  const client = member.client;
  const connection = client.connection;
  var sql_command = "INSERT INTO WDR.OLD_USER_ROLES (discord_id, roles) VALUES (\'" + member.id + "\',\'" + JSON.stringify(Array.from(member.roles.cache.keys())) + "\')";
  connection.query(sql_command, function(error, result, fields){
    if(error) { throw error }
  })
  const channel = member.guild.channels.cache.find(channel => channel.name === "logs");
  if (!channel) return;
  const embed = new Discord.MessageEmbed()
  .setAuthor(`${member.user.tag}  ${member}`,`${member.user.displayAvatarURL()}`)
  .setFooter("Leave")
  .setTimestamp()
  .setColor(0xB30000);
  member.guild.channels.cache.get(channel.id).send({embed});
};
