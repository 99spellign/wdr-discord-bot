const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const removeLoc = require("../util/removeLoc");
const config = require("../config.json");

exports.run = function(client, message, messageArg){
    var europe = message.guild.roles.cache.get(config.europe);

    if (message.member.roles.cache.has(europe.id)) {
        removeLoc(message);
        dmDelete(message, config.notifLocRem);
        return;
    }
    removeLoc(message);
    message.member.roles.add(europe).catch(console.error);
    otherEmbed(message, "europe", "changed region to");
    dmDelete(message, config.notifLocMod);
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "europe",
    description: "assigns the europe role",
    usage: "europe"
};