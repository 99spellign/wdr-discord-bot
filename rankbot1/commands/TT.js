const dmDelete = require("../util/dmDelete");

exports.run = function(client, message, messageArg){
    var tob_mentor = message.guild.roles.cache.get("520750027405262858");
    var tt = message.guild.roles.cache.get("701398342126665729");

    if (message.member.roles.cache.has(tob_mentor.id)) {
        if(!message.member.roles.cache.has(tt.id)){
            message.member.roles.add(tt).catch(console.error);
            dmDelete(message, "Tob Trialer role added\n use !tt command in <#521409656489377813> again to remove");
        }else{
            message.member.roles.remove(tt).catch(console.error);
            dmDelete(message, "ToB Trialer role removed\n you will no longer be pinged for ToB trials");
        }
        return;
    }else if(message.member.roles.cache.has(tt.id)){
        message.member.roles.remove(tt).catch(console.error);
        dmDelete(message, "ToB Trialer role removed\n you will no longer be pinged for ToB trials");
    }else{
        message.author.send("You must be a ToB mentor to assign this role")
		message.delete();
    }

};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["tt"],
    permLevel: 1
};

exports.help = {
    name: "TT",
    description: "ToB Trialer role",
    usage: "TT"
};
