const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var events = message.guild.roles.cache.get(config.events);
    if (!message.member.roles.cache.has(events.id)) {
        otherEmbed(message, "Event", "added");
        message.member.roles.add(events).catch(console.error);
        dmDelete(message, "**Event** " + config.notifRoleAdd);
    } else {
        otherEmbed(message, "Event", "removed");
        message.member.roles.remove(events).catch(console.error);
        dmDelete(message, "**Event** " + config.notifRoleRem);
    }

};

exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "event",
    description: "assigns the event role",
    usage: "event"
};