const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const removeCox = require("../util/removeCox");
const removeTob = require("../util/removeTob");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    let nightmare = message.guild.roles.cache.get(config.nightmare);
    let scouttrader = message.guild.roles.cache.get("828750677701558312");
    var nonraider = message.guild.roles.cache.get(config.nonraider);
    if (!message.member.roles.cache.has(nonraider.id)) {
        if(message.member.roles.cache.has(nightmare.id)){
            message.member.roles.remove(nightmare).catch(console.error);
        }
        if(message.member.roles.cache.has(scouttrader.id)){
            message.member.roles.remove(scouttrader).catch(console.error);
        }
        otherEmbed(message, "nonraider", "added");
        message.member.fetch();
        removeCox(message);
        removeTob(message);
        message.member.roles.add(nonraider).catch(console.error);
        dmDelete(message, "**nonraider** " + config.notifRoleAdd);
    } else {
        otherEmbed(message, "nonraider", "removed");
        message.member.roles.remove(nonraider).catch(console.error);
        dmDelete(message, "**nonraider** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "nonraider",
    description: "assigns the nonraider role",
    usage: "nonraider"
};