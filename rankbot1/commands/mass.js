const dmDelete = require("../util/dmDelete");
const otherEmbed = require("../util/otherEmbed");
const config = require("../config.json");
exports.run = function(client, message, messageArg) {
    var mass = message.guild.roles.cache.get(config.mass);

    var tierRequirement = "Tier 1";
    if (!message.member.roles.cache.has(mass.id)) {
        if (message.member.roles.cache.has(config.cox1) || message.member.roles.cache.has(config.cox2) || message.member.roles.cache.has(config.cox3) || message.member.roles.cache.has(config.cox4) || message.member.roles.cache.has(config.cox5)) {
            otherEmbed(message, "Mass", "added");
            message.member.roles.add(mass).catch(console.error);
            dmDelete(message, "**Mass** " + config.notifRoleAdd);
        } else {
            dmDelete(message, config.errKCLow + "**" + tierRequirement + "**");
        }
    } else {
        otherEmbed(message, "Mass", "removed");
        message.member.roles.remove(mass).catch(console.error);
        dmDelete(message, "**Mass** " + config.notifRoleRem);
    }
};
exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: [],
    permLevel: 0
};

exports.help = {
    name: "mass",
    description: "assigns the mass role",
    usage: "mass"
};