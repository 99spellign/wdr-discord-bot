const config = require("../config.json");

module.exports = function removeLoc(message) {
    let oceania = message.guild.roles.cache.get(config.oceania);
    let uswest = message.guild.roles.cache.get(config.uswest);
    let useast = message.guild.roles.cache.get(config.useast);
    let europe = message.guild.roles.cache.get(config.europe);

    message.member.roles.remove(oceania).catch(console.error);
    message.member.roles.remove(uswest).catch(console.error);
    message.member.roles.remove(useast).catch(console.error);
    message.member.roles.remove(europe).catch(console.error);
};